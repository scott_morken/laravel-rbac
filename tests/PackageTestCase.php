<?php

use Mockery as m;

class PackageTestCase extends Illuminate\Foundation\Testing\TestCase
{

    protected $mock;

    protected $useDatabase = false;

    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__ . '/../../../../bootstrap/start.php';
    }

    public function setUp()
    {
        parent::setUp();
        if ($this->useDatabase) {
            $this->setupDb();
        }
    }

    public function tearDown()
    {
        m::close();
    }

    /**
     * Use in setUp to mock the interface: $this->mock = $this->mock('Early\SomeInterface');
     * @param string $class
     * @return m\MockInterface|Yay_MockObject
     */
    protected function mock($class)
    {
        $mock = m::mock($class);
        $this->app->instance($class, $mock);
        return $mock;
    }

    protected function setupDb()
    {
        Artisan::call('migrate', array('--bench' => 'smorken/rbac'));
        Artisan::call('db:seed', array('--class' => 'RbacSeeder'));
    }

    /**
     * Not really needed with sqlite memory database
     */
    protected function tearDownDb()
    {
        Artisan::call('migrate:reset');
    }

}
