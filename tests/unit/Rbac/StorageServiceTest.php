<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 10:20 AM
 */
use Mockery as m;
use Smorken\Rbac\StorageService;

class StorageServiceTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Rbac\StorageService
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $roles;

    /**
     * @var Mockery\Mock
     */
    protected $permissions;

    public function tearDown()
    {
        m::close();
    }

    public function setUp()
    {
        $this->roles = m::mock('\Smorken\Rbac\Model\RolesInterface');
        $this->permissions = m::mock('\Smorken\Rbac\Model\PermissionsInterface');
        $this->sut = new StorageService($this->roles, $this->permissions);
    }

    public function testGetRolesProvider()
    {
        $rp = $this->sut->rolesProvider();
        $this->assertInstanceOf('\Smorken\Rbac\Model\RolesInterface', $rp);
    }

    public function testGetRolesProviderNew()
    {
        $rp = $this->sut->rolesProvider(true);
        $this->assertNotSame($rp, $this->sut->rolesProvider());
    }

    public function testGetPermissionsProvider()
    {
        $p = $this->sut->permissionsProvider();
        $this->assertInstanceOf('\Smorken\Rbac\Model\PermissionsInterface', $p);
    }

    public function testGetPermissionsProviderNew()
    {
        $p = $this->sut->permissionsProvider(true);
        $this->assertNotSame($p, $this->sut->rolesProvider());
    }
}