<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:33 AM
 */

use Mockery as m;
use Smorken\Rbac\RbacService;
use Illuminate\Support\Facades\Auth;

class RbacServiceTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Rbac\RbacService
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $roles;

    /**
     * @var Mockery\Mock
     */
    protected $permissions;

    /**
     * @var Smorken\Rbac\StorageService
     */
    protected $storageService;

    public function tearDown()
    {
        m::close();
    }

    public function setUp()
    {
        parent::setUp();
        $this->roles = m::mock('\Smorken\Rbac\Model\RolesInterface');

        $this->permissions = m::mock('\Smorken\Rbac\Model\PermissionsInterface');
        $this->permissions->shouldReceive('all')
            ->andReturn($this->mockPermissions());
        $this->permissions->shouldReceive('name')
            ->andReturnUsing(function($model) {
                return $model->name;
            });
        $this->permissions->shouldReceive('id')
            ->andReturnUsing(function($model) {
                    return $model->id;
                }
            );
        $this->roles->shouldReceive('name')
            ->andReturnUsing(function($model) {
                return $model->name;
            });
        $this->roles->shouldReceive('id')
            ->andReturnUsing(function($model) {
                    return $model->id;
                }
            );
        $this->roles->shouldReceive('isSuperAdmin')
            ->andReturnUsing(function($model) {
                    return $model->super_admin == 1;
                }
            );
        $this->roles->shouldReceive('inheritsFrom')
            ->andReturnUsing(function($model) {
                return $model->inheritance;
            });
        $this->storageService = new Smorken\Rbac\StorageService($this->roles, $this->permissions);

        $this->user = m::mock('StdClass');
        $this->user->shouldReceive('getAuthIdentifier')
            ->andReturn(1);
        Auth::shouldReceive('user')
            ->andReturn($this->user);

//        Auth::shouldReceive('user')
//            ->andReturnNull(true);
//        Auth::shouldReceive('user->getAuthIdentifier')
//            ->andReturn(1);
        $this->sut = new RbacService($this->storageService);
    }

    public function testAllRolesCallsLoadAll()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn(array());
        $this->assertEquals(array(), $this->sut->allRoles());
    }

    public function testUserRolesCallsLoadUser()
    {
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array());
        $this->assertEquals(array(), $this->sut->userRoles());
    }

    public function testUserIdChangesRecallsLoadUser()
    {
        $this->sut->setUserId(2);

        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array());

        $this->assertEquals(array(), $this->sut->userRoles());
    }

    public function testRecursiveInheritance()
    {
        $inheritance = $this->mockInheritanceItems();
        $this->sut->recursiveInheritance(array_pop($inheritance), $inheritance);
        $result = $this->sut->inheritance();
        $this->assertEquals(2, count($result[3]));
        $this->assertEquals(1, count($result[2]));
    }

    protected function mockInheritanceItems()
    {
        return array(
            /*1 => $this->createInheritanceItem(1, 'top'),*/
            2 => $this->createInheritanceItem(2, 'mid'),
            3 => $this->createInheritanceItem(3, 'bottom'),
        );
    }

    protected function createInheritanceItem($id, $name)
    {
        $o = new \stdClass();
        $o->id = $id;
        $o->name = $name;
        return $o;
    }

    public function testUserHasRoleByInteger()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1, 2));
        $this->assertTrue($this->sut->hasRole(2));
    }

    public function testUserHasRoleByIntegerFalse()
    {

        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(2));
        $this->assertFalse($this->sut->hasRole(1));
    }

    public function testUserHasRoleByName()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1, 2));
        $this->assertTrue($this->sut->hasRole('top'));
    }

    public function testUserHasRoleByNameFalse()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(2));
        $this->assertFalse($this->sut->hasRole('bottom'));
    }

    public function testUserHasRoleByNameInherited()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(3));
        $this->assertTrue($this->sut->hasRole('mid'));
    }

    public function testUserHasRoleByNameFalseLazyLoadsUserRoles()
    {
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles());
        $this->roles->shouldReceive('loadUserRoles')
            ->never();
        $this->assertFalse($this->sut->hasRole('baz'));
    }

    public function testUserHasPermissionByIntegerSuperAdmin()
    {
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1));
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles(true));
        $this->assertTrue($this->sut->hasPermission(1, 1));
    }

    public function testUserHasPermissionByInteger()
    {
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(2));
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles(true));
        $this->assertTrue($this->sut->hasPermission(2, 2));
    }

    public function testUserHasPermissionByIntegerFalse()
    {

        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(3));
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles(true));
        $this->assertFalse($this->sut->hasPermission(3, 3));
    }

    public function testUserHasPermissionByName()
    {
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1));
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles(true));
        $this->assertTrue($this->sut->hasPermission(1, 'view'));
    }

    public function testUserHasPermissionByNameFalse()
    {
        $this->roles->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1, 2));
        $this->roles->shouldReceive('loadAllWithPermissions')
            ->once()
            ->andReturn($this->mockRoles(true));
        $this->assertFalse($this->sut->hasPermission(2, 'view'));
    }

    protected function mockRoles($permissions = false)
    {
        return array(
            1 => $this->createRoleMock(1, 'top', false, $permissions, 1, 4),
            2 => $this->createRoleMock(2, 'mid', false, $permissions, 2, 3),
            3 => $this->createRoleMock(3, 'bottom', true, $permissions, 1, 2),
        );
    }

    protected function createRoleMock($id, $name, $inherits, $permissions = false, $start = 1, $end = 4)
    {
        $m = m::mock('\Smorken\Rbac\Model\RolesInterface');
        $m->id = $id;
        $m->name = $name;
        if ($id === 1) {
            $m->super_admin = 1;
        }
        else {
            $m->super_admin = 0;
        }

        $m->inheritance = ($inherits ? array_reverse($this->mockInheritanceItems()) : array());

        $m->shouldReceive('permissions')
            ->andReturn($permissions ? $this->mockPermissions($start, $end) : array());

        return $m;
    }

    protected function mockPermissions($start = 1, $end = 4)
    {
        $perms = array(
            1 => 'view',
            2 => 'create',
            3 => 'update',
            4 => 'delete',
        );
        $result = array();
        for($i = $start; $i <= $end; $i ++ ) {
            $result[$i] = $this->createPermissionMock($i, $perms[$i]);
        }

        return $result;
    }

    protected function createPermissionMock($id, $name)
    {
        $m = m::mock('\Smorken\Rbac\Model\PermissionsInterface');
        $m->id = $id;
        $m->name = $name;
        return $m;
    }
} 