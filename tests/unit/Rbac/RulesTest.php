<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/20/15
 * Time: 12:05 PM
 */

use Smorken\Rbac\Rules;
use Mockery as m;

class RulesTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Rules
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $guard;

    /**
     * @var Mockery\Mock
     */
    protected $rbac;

    public function setUp()
    {
        $this->guard = m::mock('Illuminate\Contracts\Auth\Guard');
        $this->rbac = m::mock('Smorken\Rbac\RbacService');
        $this->sut = new Rules($this->guard, $this->rbac);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testUserRulesReturnsTrue()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('*'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => '*'
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id1');
        $this->assertTrue($this->sut->check('getIndex', $rules));
    }

    public function testUserWithActionRulesReturnsTrue()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('getIndex'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => '*'
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id1');
        $this->assertTrue($this->sut->check('getIndex', $rules));
    }

    public function testUserWithActionNotCurrentRulesReturnsFalseForDeny()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('getCreate'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => array(
                'users' => array('*')
            ),
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id1');
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }

    public function testUserWithActionNotCurrentRulesReturnsTrueForDenyAuthenticated()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('getCreate'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => '@'
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id3');
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }

    public function testUserRulesReturnsFalseForAllowGuest()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('*'),
                'users' => array('?'),
            ),
            'deny' => '*'
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id3');
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }

    public function testUserWithActionRulesReturnsFalse()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('getIndex'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => array(
                'users' => array('*')
            ),
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id3');
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }

    public function testUserRulesReturnsFalse()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('*'),
                'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => array(
                'users' => array('*')
            ),
        );
        $this->guard->shouldReceive('check')->andReturn(true);
        $this->guard->shouldReceive('user->getAuthIdentifier')->andReturn('user_id3');
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }

    public function testRoleRulesReturnsTrue()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('*'),
                'roles' => array('foo'),
            ),
            'deny' => '*'
        );

        $this->rbac->shouldReceive('hasRole')->with('foo')->andReturn(true);
        $this->assertTrue($this->sut->check('getIndex', $rules));
    }

    public function testRoleWithActionRulesReturnsTrue()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('getIndex'),
                'roles' => array('foo'),
            ),
            'deny' => '*'
        );

        $this->rbac->shouldReceive('hasRole')->with('foo')->andReturn(true);
        $this->assertTrue($this->sut->check('getIndex', $rules));
    }

    public function testRoleRulesReturnsFalse()
    {
        $rules = array(
            'allow' => array(
                'actions' => array('*'),
                'roles' => array('foo'),
            ),
            'deny' => '*'
        );

        $this->rbac->shouldReceive('hasRole')->with('foo')->andReturn(false);
        $this->assertFalse($this->sut->check('getIndex', $rules));
    }
}