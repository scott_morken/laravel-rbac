<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/29/14
 * Time: 9:24 AM
 */

use Mockery as m;
use Smorken\Rbac\Middleware\Rbac as RbacFilter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Smorken\Rbac\Facades\Rbac;

class RbacFilterTest extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Rbac\Filters\RbacFilter;
     */
    protected $sut;

    /**
     * @var Mockery/Mock
     */
    protected $route;

    public function setUp()
    {
        $r = m::mock('StdClass');
        $r->shouldReceive('getActionName')
            ->andReturn('SomeController@getAction');
        $auth = m::mock('Illuminate\Contracts\Auth\Guard');
        $this->sut = new RbacFilter($auth);
        $this->sut->setCurrentAction($r);
        $this->route = $r;
    }

    public function tearDown()
    {
        m::close();
    }

    public function testCheckAllowedSingleLevel()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('some-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('some-role')
            ->once()
            ->andReturn(true);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedMultiple()
    {
        $rules['allow'] = array(
            array('actions' => array('noAction'), 'roles' => array('no-role')),
            array('actions' => array('getAction', 'postAction'), 'roles' => array('has-role'))
        );
        Rbac::shouldReceive('hasRole')
            ->with('has-role')
            ->once()
            ->andReturn(true);
        Rbac::shouldReceive('hasRole')
            ->with('no-role')
            ->never();
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedSingleLevelNullOnNoMatch()
    {
        $rules['allow'] = array(
            'actions' => array('postAction'),
            'roles' => array('no-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->never();
        $this->assertNull($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedSingleLevelFalse()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('no-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('no-role')
            ->once()
            ->andReturn(false);
        $this->assertFalse($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedMultipleLevelNullOnNoMatch()
    {
        $rules['allow'] = array(
            array(
                'actions' => array('postAction'),
                'roles' => array('other-role'),
            ),
            array(
                'action' => array('getIndex', 'getCreate'),
                'roles' => array('some-role'),
            )
        );
        Rbac::shouldReceive('hasRole')
            ->never();
        $this->assertNull($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedMultipleLevelFalse()
    {
        $rules['allow'] = array(
            array(
                'actions' => array('getAction', 'postAction'),
                'roles' => array('some-role'),
            ),
            array(
                'actions' => array('getIndex', 'getCreate'),
                'roles' => array('other-role'),
            ),
        );
        Rbac::shouldReceive('hasRole')
            ->with('some-role')
            ->once()
            ->andReturn(false);
        Rbac::shouldReceive('hasRole')
            ->with('other-role')
            ->never();
        $this->assertFalse($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedMultipleRoles()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('has-role', 'no-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('has-role')
            ->once()
            ->andReturn(true);
        Rbac::shouldReceive('hasRole')
            ->with('no-role')
            ->never();
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedMultipleRolesCanCheckFalseBeforeTrue()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('no-role', 'has-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('has-role')
            ->once()
            ->andReturn(true);
        Rbac::shouldReceive('hasRole')
            ->with('no-role')
            ->once()
            ->andReturn(false);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedPermissions()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'permissions' => array('has-role', 'some-permission'),
        );
        Rbac::shouldReceive('hasPermission')
            ->with('has-role', 'some-permission')
            ->once()
            ->andReturn(true);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedPermissionsMultiplePermissions()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'permissions' => array(
                array('no-role', 'other-permission'),
                array('has-role', 'some-permission')
            ),
        );
        Rbac::shouldReceive('hasPermission')
            ->with('no-role', 'other-permission')
            ->once()
            ->andReturn(false);
        Rbac::shouldReceive('hasPermission')
            ->with('has-role', 'some-permission')
            ->once()
            ->andReturn(true);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckAllowedRouteIsWildcard()
    {
        $rules['allow'] = array(
            'actions' => array('*'),
            'roles' => 'has-role',
        );
        Rbac::shouldReceive('hasRole')
            ->with('has-role')
            ->andReturn(true);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckRoleSpecialCharacterAnyUser()
    {
        $rules['allow'] = array(
            'actions' => array('getAction'),
            'roles' => '*',
        );
        Rbac::shouldReceive('hasRole')->never();
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckRoleSpecialCharacterGuestOnly()
    {
        $rules['allow'] = array(
            'actions' => array('getAction'),
            'roles' => '?',
        );
        Rbac::shouldReceive('hasRole')->never();
        Auth::shouldReceive('guest')->once()->andReturn(true);
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckRoleSpecialCharacterAuthOnly()
    {
        $rules['allow'] = array(
            'actions' => array('getAction'),
            'roles' => '@',
        );
        Rbac::shouldReceive('hasRole')->never();
        Auth::shouldReceive('check')->once()->andReturn(true);
        Auth::shouldReceive('user')->once()->andReturn(new \stdClass());
        $this->assertTrue($this->sut->checkAllowed($rules));
    }

    public function testCheckDeniedSingleLevel()
    {
        $rules['deny'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('some-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('some-role')
            ->once()
            ->andReturn(true);
        $this->assertTrue($this->sut->checkDenied($rules));
    }

    public function testCheckDeniedWildcardIsTrue()
    {
        $rules['deny'] = '*';
        Rbac::shouldReceive('hasRole')
            ->never();
        $this->assertTrue($this->sut->checkDenied($rules));
    }

    public function testFilterIsAllowedByAllow()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('some-role'),
        );
        $rules['deny'] = '*';
        Rbac::shouldReceive('hasRole')
            ->with('some-role')
            ->once()
            ->andReturn(true);
        $this->assertNull($this->sut->filter($this->route, null, $rules));
    }

    public function testFilterIsDeniedByDeny()
    {
        $rules['allow'] = array(
            'actions' => array('postAction'),
            'roles' => array('some-role'),
        );
        $rules['deny'] = '*';
        Rbac::shouldReceive('hasRole')->never();
        App::shouldReceive('abort')->once()->with(403, m::type('string'));
        $this->assertNull($this->sut->filter($this->route, null, $rules));
    }

    public function testFilterIsDeniedByAllow()
    {
        $rules['allow'] = array(
            'actions' => array('getAction', 'postAction'),
            'roles' => array('some-role'),
        );
        $rules['deny'] = array(
            'actions' => array('getAction'),
            'roles' => array('no-role'),
        );
        Rbac::shouldReceive('hasRole')
            ->with('some-role')
            ->once()
            ->andReturn(false);
        Rbac::shouldReceive('hasRole')->with('no-role')->never();
        App::shouldReceive('abort')->once()->with(403, m::type('string'));
        $this->assertNull($this->sut->filter($this->route, null, $rules));
    }

    public function testFilterIsDeniedWith401ByAllowAuthenticated()
    {
        $rules['allow'] = array(
        'actions' => array('getAction', 'postAction'),
        'roles' => '@',
        );
        $rules['deny'] = '*';
        Auth::shouldReceive('check')->once()->andReturn(false);
        Rbac::shouldReceive('hasRole')->never();
        App::shouldReceive('abort')->once()->with(401, m::type('string'));
        $this->assertNull($this->sut->filter($this->route, null, $rules));
    }
} 