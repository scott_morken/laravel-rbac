<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/9/14
 * Time: 7:41 AM
 */
require __DIR__ . '/../../PackageTestCase.php';

use Mockery as m;
use Smorken\Rbac\RbacService;

/**
 * Class RbacServiceTest
 * Roles:
 * 1 - super admin
 * 2 - admin
 * 3 - manage
 * 4 - user
 */
class RbacServiceTest extends PackageTestCase {

    protected $useDatabase = true;

    /**
     * @var RbacService
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $rp;

    public function setUp()
    {
        parent::setUp();
        $this->rp = m::mock('\Smorken\Rbac\Storage\Ardent\Role[loadUserRoles]', array(
            new Smorken\Rbac\Model\Ardent\Role()
        ));
        $pp = new Smorken\Rbac\Storage\Ardent\Permission(new Smorken\Rbac\Model\Ardent\Permission());
        $ss = new Smorken\Rbac\StorageService($this->rp, $pp);
        \Auth::shouldReceive('user->getAuthIdentifier')
            ->andReturn(1);
        $this->sut = new RbacService($ss);
    }

    public function testUserHasRoleByInteger()
    {
        $this->rp->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1, 2));
        $this->assertTrue($this->sut->hasRole(1));
    }

    public function testUserHasRoleByName()
    {
        $this->rp->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(2));
        $this->assertTrue($this->sut->hasRole('admin'));
    }

    public function testUserHasRoleByNameFalse()
    {
        $this->rp->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(3));
        $this->assertFalse($this->sut->hasRole('admin'));
    }

    public function testUserHasRoleByNameInherits()
    {
        $this->rp->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(2));
        $this->assertFalse($this->sut->hasRole('manage'));
    }

    public function testSuperAdminHasRole()
    {
        $this->rp->shouldReceive('loadUserRoles')
            ->once()
            ->with(1)
            ->andReturn(array(1));
        $this->assertTrue($this->sut->hasRole('user'));
    }
} 