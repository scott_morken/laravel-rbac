<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:27 AM
 */
return [
    'connection'    => 'default',
    'route_prefix'  => 'admin/rbac',
    'layout'        => 'smorken/rbac::layouts.master',
    'roles'         => [
        'storage'    => \Smorken\Rbac\Storage\EloquentRoleRepository::class,
        'model'      => \Smorken\Rbac\Model\Eloquent\Role::class,
        'controller' => \Smorken\Rbac\Controllers\RoleController::class,
    ],
    'permissions'   => [
        'storage'    => \Smorken\Rbac\Storage\EloquentPermissionRepository::class,
        'model'      => \Smorken\Rbac\Model\Eloquent\Permission::class,
        'controller' => \Smorken\Rbac\Controllers\PermissionController::class,
    ],
    'allowed_roles' => ['admin'],
];
