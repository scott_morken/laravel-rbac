<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 7:53 AM
 */
\Route::group(
    [
        'middleware' => ['auth', 'rbac'],
        'prefix'     => config('smorken/rbac::config.route_prefix', 'admin/rbac'),
        'rbac'       => [
            'allow' => [
                'actions' => ['*'],
                'roles'   => ['admin'],
            ],
            'deny'  => [
                '*',
            ],
        ],
    ],
    function () {
        \Route::controller('roles', config('smorken/rbac::config.roles.controller'));
        \Route::controller('permissions', config('smorken/rbac::config.permissions.controller'));
    }
);
