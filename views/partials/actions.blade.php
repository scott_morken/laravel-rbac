<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 9:06 AM
 */
/* @var string $id_key
 * @var string $id
 */
?>
<span class="actions">
    @foreach($actions as $action)
    <a href="{{ action($action['action']) }}" class="action">
        <span class="glyphicon {{ $action['icon'] }}"></span>
    </a>
    @endforeach
</span>
