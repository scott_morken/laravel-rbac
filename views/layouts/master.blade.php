<html>
<head>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/css/bootstrap.min.css" />

    @if (!empty($favicon))
    <link rel="icon" {{ !empty($faviconType) ? 'type="$faviconType"' : '' }} href="{{ $favicon }}" />
    @endif

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <title>{{ (!empty($siteName)) ? $siteName : "RBAC"}} - {{isset($title) ? $title : '' }}</title>
</head>
<body>
@include('includes.header')
{{ isset($breadcrumb) ? Breadcrumbs::create($breadcrumb) : ''; }}
<div id="content">
    @yield('content')
</div>
</body>
</html>