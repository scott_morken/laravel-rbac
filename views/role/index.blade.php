<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 8:51 AM
 */
/* @var Smorken\Rbac\Model\RolesInterface[] $roles */
?>
@section('content')
{{ HTML::tableView(
    $roles,
    array(
        'id' => 'ID',
        'role_name' => 'Name',
        'description' => 'Description',
        'inherit_from' => 'Inherits',
        'super_admin' => array(
            'label' => 'Super Admin',
            'value' => function($m) { return $m->super_admin == 1 ? 'Yes' : 'No'; },
        ),
        'permissions' => array(
            'label' => 'Permissions',
            'value' => function($m) {
                $permissions = array();
                foreach($m->permissions as $p) {
                    $permissions[] = $p->permission_name;
                }
                return implode(', ', $permissions);
            },
        ),
    ),
    array('class' => 'table-striped'),
    \Config::get('smorken/rbac::config.roles.controller')
) }}

@stop