<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 8:51 AM
 */
?>
@if(count($permissions))
@foreach($permissions as $permission)
    <div>
        {{ $permission->id }}:
        {{ $permission->permission_name }}
        <small>{{ $permission->description }}</small>
    </div>
@endforeach
@else
<div>None</div>
@endif