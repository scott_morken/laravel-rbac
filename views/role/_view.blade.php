<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 3:30 PM
 */
?>
<h2>Role #{{ $model->id }}</h2>
{{ HTML::detailView($model, array(
    'id' => 'ID',
    'role_name' => 'Role',
    'description' => 'Description',
    'inherit_from' => 'Inherits',
    'super_admin' => array(
        'label' => 'Super Admin',
        'value' => $model->super_admin == 1 ? 'Yes' : 'No',
    ),
    'permissions' => array(
        'label' => 'Permissions',
        'raw' => true,
        'value' => \View::make('smorken/rbac::role._view_permissions', array(
                'permissions' => $model->permissions
            )
        )
    ),
)) }}