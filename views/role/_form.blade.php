<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 3:30 PM
 */
?>
@section('content')
{{ Strap::model($model, array('class' => 'form-horizontal')) }}
<h2>
@if($model->id)
    Update role #{{ $model->id }}
    @else
    Create new role
    @endif
</h2>
@if($errors->has())
    @foreach($errors->all() as $message)
<p class="alert alert-danger">{{ $message }}</p>
@endforeach
@endif
<?php Strap::type('horizontal'); ?>
{{ Strap::text('role_name')->label('Role')->options('autofocus') }}
{{ Strap::text('description')->label('Description') }}
{{ Form::hidden('super_admin', 0) }}
{{ Strap::checkbox('super_admin')->label('Super Admin')->inside() }}
{{ Strap::select('inherit_from')->label('Inherits')->data($model->inheritsFromList()) }}
<div class="form-group">
    <span class="control-label col-lg-2 col-sm-4">
        <b>Permissions</b>
    </span>
    <div class="col-lg-10 col-sm-8">
        @foreach($permissions as $permission)
        {{ Strap::checkbox("permissions_list[]")
            ->label($permission->permission_name)
            ->value($permission->id)
            ->inside()
            ->type('default') }}
        @endforeach
    </div>
</div>
{{ Strap::group(array(
    Strap::submit('Save'),
    Strap::link(action('Smorken\Rbac\Controllers\RoleController@getIndex'), 'Cancel')->buttonify('btn-danger'),
))->label(false)->type('horizontal') }}

{{-- Former::actions()->large_primary_submit('Save')->large_default_link('Cancel', action('Admin\Organization\OrganizationController@getIndex')) --}}
{{ Strap::close() }}
@stop