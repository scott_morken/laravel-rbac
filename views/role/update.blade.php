<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 3:29 PM
 */
?>
@section('content')
    @include('smorken/rbac::role._form', array('model' => $model))
@stop