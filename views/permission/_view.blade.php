<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 3:30 PM
 */
?>
<h2>Permission #{{ $model->id }}</h2>
{{ HTML::detailView($model, array(
    'id' => 'ID',
    'permission_name' => 'Permission',
    'description' => 'Description',
)) }}