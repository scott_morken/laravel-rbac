<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/23/14
 * Time: 12:09 PM
 */
?>
@section('content')
{{ HTML::tableView(
    $models,
    array(
        'id' => 'ID',
        'permission_name' => 'Name',
        'description' => 'Description',
    ),
    array('class' => 'table-striped'),
    \Config::get('smorken/rbac::config.permissions.controller')
) }}
@stop