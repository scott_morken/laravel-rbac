<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 3:30 PM
 */
?>
@section('content')
{{ Strap::model($model, array('class' => 'form-horizontal')) }}
<h2>
@if($model->id)
    Update permission #{{ $model->id }}
    @else
    Create new permission
    @endif
</h2>
@if($errors->has())
    @foreach($errors->all() as $message)
<p class="alert alert-danger">{{ $message }}</p>
@endforeach
@endif
<?php Strap::type('horizontal'); ?>
{{ Strap::text('permission_name')->label('Permission')->options('autofocus') }}
{{ Strap::text('description')->label('Description') }}
{{ Strap::group(array(
    Strap::submit('Save'),
    Strap::link(action('Smorken\Rbac\Controllers\PermissionController@getIndex'), 'Cancel')->buttonify('btn-danger'),
))->label(false)->type('horizontal') }}

{{ Strap::close() }}
@stop