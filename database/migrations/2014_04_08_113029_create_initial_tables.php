<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitialTables extends Migration
{

    protected $tables = [
        'roles',
        'permissions',
        'role_user',
        'permission_role',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            $this->upTable($table);
        }
    }

    protected function upTable($name)
    {
        $methods = [
            '_create',
            '_index',
            '_data',
            '_foreignkey',
        ];
        foreach ($methods as $m) {
            $methodname = $name . $m;
            if (method_exists($this, $methodname)) {
                $this->$methodname($name);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::drop($table);
        }
    }

    protected function roles_create($name)
    {
        Schema::create(
            $name,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('role_name', 16)->unique();
                $table->string('description', 64)->nullable();
                $table->string('inherit_from', 16)->nullable();
                $table->boolean('super_admin')->default(false);
                $table->timestamps();
            }
        );
    }

    protected function roles_index($name)
    {
        Schema::table(
            $name,
            function (Blueprint $table) {
                $table->index('inherit_from', 'r_inherit_from_ndx');
            }
        );
    }

    protected function permissions_create($name)
    {
        Schema::create(
            $name,
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('permission_name', 16)->unique();
                $table->string('description', 64)->nullable();
                $table->timestamps();
            }
        );
    }

    protected function role_user_create($name)
    {
        Schema::create(
            $name,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    protected function role_user_index($name)
    {
        Schema::table(
            $name,
            function (Blueprint $table) {
                $table->index('user_id', 'ru_user_id_ndx');
            }
        );
    }

    protected function role_user_foreignkey($name)
    {
        Schema::table(
            $name,
            function (Blueprint $table) {
                $table->foreign('role_id')->references('id')->on('roles');
            }
        );
    }

    protected function permission_role_create($name)
    {
        Schema::create(
            $name,
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('role_id')->unsigned();
                $table->integer('permission_id')->unsigned();
            }
        );
    }

    protected function permission_role_foreignkey($name)
    {
        Schema::table(
            $name,
            function (Blueprint $table) {
                $table->foreign('role_id')->references('id')->on('roles');
                $table->foreign('permission_id')->references('id')->on('permissions');
            }
        );
    }
}
