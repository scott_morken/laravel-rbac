<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 11:47 AM
 */

use Smorken\Rbac\Model\Eloquent\Permission;

class PermissionTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        \Illuminate\Support\Facades\DB::table('permissions')->delete();

        Permission::create(
            [
                'permission_name' => 'view',
                'description'     => 'View Records',
            ]
        );
        Permission::create(
            [
                'permission_name' => 'create',
                'description'     => 'Create Records',
            ]
        );
        Permission::create(
            [
                'permission_name' => 'update',
                'description'     => 'Update Records',
            ]
        );
        Permission::create(
            [
                'permission_name' => 'delete',
                'description'     => 'Delete Records',
            ]
        );
    }
} 
