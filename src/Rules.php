<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/20/15
 * Time: 11:55 AM
 */

namespace Smorken\Rbac;

use Illuminate\Contracts\Auth\Guard;

class Rules
{

    protected $code = 403;

    protected $message = 'You do not appear to have access to this resource.';

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var RbacService
     */
    protected $rbac;

    protected $action;

    /**
     * @param Guard $auth
     * @param RbacService $rbac
     */
    public function __construct(Guard $auth, RbacService $rbac)
    {
        $this->rbac = $rbac;
        $this->auth = $auth;
    }

    public function check($action, $rules)
    {
        $this->action = $action;
        $allowed = $this->checkAllowed($rules);
        if ($allowed === null) {
            $allowed = !$this->checkDenied($rules);
        }
        return $allowed;
    }

    public function checkAllowed($rules)
    {
        $allowed = null;
        if (isset($rules['allow'])) {
            $routerules = $this->findRouteRules($rules['allow']);
            if ($routerules) {
                $allowed = $this->checkRouteRulesAllowed($routerules);
            }
        }
        return $allowed;
    }

    protected function findRouteRules($rules)
    {
        $routerules = [];
        if (isset($rules['actions'])) {
            $rules = [$rules];
        }
        foreach ($rules as $ruleset) {
            if (isset($ruleset['actions']) && ($ruleset['actions'] == '*' || in_array('*', $ruleset['actions']))) {
                $routerules[] = $ruleset;
                continue;
            }
            if (isset($ruleset['actions']) && in_array($this->action, $ruleset['actions'])) {
                $routerules[] = $ruleset;
            }
            if (!isset($ruleset['actions'])) {
                $routerules[] = $rules;
                break;
            }
        }
        return $routerules;
    }

    protected function checkRouteRulesAllowed($rules)
    {
        $allowed = false;
        foreach ($rules as $rule) {
            $allowed = $this->checkRouteRule($rule);
            if (!$allowed) {
                break;
            }
        }
        return $allowed;
    }

    protected function checkRouteRule($rule)
    {
        if (isset($rule['users'])) {
            return $this->checkUser($rule['users']);
        }
        if (isset($rule['roles'])) {
            $meetsrule = $this->checkRoles($rule['roles']);
        } else {
            if (isset($rule['permissions'])) {
                $meetsrule = $this->checkPermissions($rule['permissions']);
            } else {
                throw new RbacException(
                    "No rule specified even though access control was requested for {$this->action}."
                );
            }
        }
        return $meetsrule;
    }

    protected function checkUser($users)
    {
        if (!is_array($users)) {
            $users = (array)$users;
        }
        if (in_array('*', $users)) {
            return true;
        }
        $user_id = $this->auth->check() ? $this->auth->user()->getAuthIdentifier() : false;
        if ($user_id) {
            return in_array($user_id, $users);
        }
        return false;
    }

    protected function checkRoles($roles)
    {
        $hasrole = false;
        if (!is_array($roles)) {
            $roles = (array)$roles;
        }
        foreach ($roles as $role) {
            if (in_array($role, ['*', '?', '@'])) {
                $hasrole = $this->checkAuthType($role);
            } else {
                $hasrole = $this->rbac->hasRole($role);
            }
            if ($hasrole) {
                break;
            }
        }
        return $hasrole;
    }

    /**
     * $type is one of '*', '?', '@'
     * * - any user, guest or authenticated
     * ? - guest user
     * @ - authenticated user
     * @param $type
     * @throws RbacException
     * @return bool
     */
    protected function checkAuthType($type)
    {
        $hastype = $this->isAuthType($type);
        if (!$hastype && $type == '@') {
            $this->code = 401;
            $this->message = 'You must be authenticated to view this resource.';
        }
        return $hastype;
    }

    /**
     * $type is one of '*', '?', '@'
     * * - any user, guest or authenticated
     * ? - guest user
     * @ - authenticated user
     * @param $type
     * @throws RbacException
     * @return bool
     */
    protected function isAuthType($type)
    {
        switch ($type) {
            case '*':
                $hastype = true;
                break;
            case '?':
                $hastype = $this->auth->guest();
                break;
            case '@':
                $hastype = $this->auth->check() && $this->auth->user();
                break;
            default:
                throw new RbacException("Unexpected user type [$type] for {$this->action}.");
        }
        return $hastype;
    }

    protected function checkPermissions($permissions)
    {
        $hasperm = false;
        if (!is_array($permissions[0])) {
            $permissions = [$permissions];
        }
        foreach ($permissions as $permission) {
            if (!is_array($permission) || count($permission) !== 2) {
                throw new RbacException("Permission check is malformed for {$this->action}.");
            }
            $hasperm = $this->rbac->hasPermission($permission[0], $permission[1]);
            if ($hasperm) {
                break;
            }
        }
        return $hasperm;
    }

    public function checkDenied($rules)
    {
        $denied = true;
        if (isset($rules['deny'])) {
            if (in_array($rules['deny'], ['*', '?', '@'])) {
                return $this->isAuthType($rules['deny']);
            }
            $routerules = $this->findRouteRules($rules['deny']);
            if ($routerules) {
                $denied = $this->checkRouteRulesDenied($routerules);
            }
        }
        return $denied;
    }

    protected function checkRouteRulesDenied($rules)
    {
        $denied = true;
        foreach ($rules as $rule) {
            $denied = $this->checkRouteRule($rule);
            if ($denied) {
                break;
            }
        }
        return $denied;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
