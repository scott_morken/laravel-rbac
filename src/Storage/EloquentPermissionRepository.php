<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:49 PM
 */

namespace Smorken\Rbac\Storage;

use Smorken\Repositories\Storage\AbstractEloquent;
use Smorken\Repositories\Storage\Contracts\Crud;

class EloquentPermissionRepository extends AbstractEloquent implements PermissionRepository, Crud
{

    public function name($model)
    {
        return $model->name();
    }
}
