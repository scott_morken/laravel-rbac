<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:49 PM
 */

namespace Smorken\Rbac\Storage;

interface RoleRepository
{

    public function isSuperAdmin($model);

    public function inheritsFrom($model);

    public function loadUserRoles($user_id);

    public function loadAllWithPermissions();

    public function getSuperAdminRole();

    public function addUsersToRole($model, array $user_ids);

    /**
     * @return PermissionsInterface[]
     */
    public function permissions();

    public function setPermissionsProvider(PermissionRepository $permissions);

    public function permissionsProvider();

    public function inheritsFromList($current = null);

    public function handlePermissions($permissions = []);
}
