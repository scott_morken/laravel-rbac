<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:49 PM
 */

namespace Smorken\Rbac\Storage;

use Illuminate\Support\Collection;
use Smorken\Rbac\Model\Contracts\Permission;
use Smorken\Rbac\Model\Eloquent\RoleUser;
use Smorken\Repositories\Storage\AbstractEloquent;
use Smorken\Repositories\Storage\Contracts\Crud;

class EloquentRoleRepository extends AbstractEloquent implements RoleRepository, Crud
{

    public function name($model)
    {
        return $model->name();
    }

    public function isSuperAdmin($model)
    {
        return $model->super_admin == 1 ? true : false;
    }

    public function inheritsFrom($model)
    {
        return $model->inheritsFrom();
    }

    public function inheritsFromList($currentId = null)
    {
        return $this->model->inheritsFromList($currentId);
    }

    public function loadUserRoles($user_id)
    {
        $key = 'rbac/user_roles-' . $user_id;
        if (!\Cache::has($key)) {
            $results = \DB::table('role_user')->where('user_id', $user_id)->lists('role_id');
            \Cache::add($key, $results, 10);
        }
        return \Cache::get($key, []);
    }

    public function loadAllWithPermissions()
    {
        $key = 'rbac/allwp';
        if (!\Cache::has($key)) {
            $results = $this->all(['with' => [['permissions']]]);
            \Cache::add($key, $results, 60);
        }
        return \Cache::get($key, []);
    }

    public function handlePermissions($permissions = [])
    {
        \Cache::flush();
        if (!$permissions) {
            $permissions = [];
        }
        return $this->model->permissions()->sync($permissions);
    }

    /**
     * @return Permission[]
     */
    public function permissions()
    {
        return $this->model->permissions;
    }

    public function roleUsers()
    {
        return $this->model->roleUsers;
    }

    public function setPermissionsProvider(PermissionRepository $permissions)
    {
        // TODO: Implement setPermissionsProvider() method.
    }

    public function permissionsProvider()
    {
        // TODO: Implement permissionsProvider() method.
    }

    /**
     * Delete an existing entity
     *
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        $m = $this->find($id);
        if ($m) {
            \Cache::flush();
            $m->permissions()->detach();
            $m->roleUsers()->delete();
            return $m->delete();
        }
    }

    public function getSuperAdminRole()
    {
        $key = 'rbac/sa';
        if (!\Cache::has($key)) {
            $results = $this->getModel()->where('super_admin', '=', 1)->first();
            \Cache::add($key, $results, 60);
        }
        return \Cache::get($key, null);
    }

    public function addUsersToRole($model, array $user_ids)
    {
        \Cache::flush();
        $current = $model->roleUsers()->lists('user_id');
        foreach ($user_ids as $user_id) {
            $in = false;
            if ($current instanceof Collection) {
                $in = $current->search($user_id);
            } else {
                if (is_array($current)) {
                    $in = in_array($user_id, $current);
                }
            }
            if (!$in) {
                $r = $model->roleUsers()->save(new RoleUser(['user_id' => $user_id]));
                if (!$r) {
                    $this->errors()->add('add_users', "Unable to add user [$user_id].");
                }
            }
        }
    }
}
