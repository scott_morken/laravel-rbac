<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:22 AM
 */

namespace Smorken\Rbac\Facades;

use Illuminate\Support\Facades\Facade;

class Rbac extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'rbac';
    }
}
