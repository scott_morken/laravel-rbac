<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:51 PM
 */

namespace Smorken\Rbac\Model\Contracts;

use Smorken\Repositories\Model\Contracts\Model;

/**
 * Interface Permission
 * @package Smorken\Rbac\Model\Contracts
 * @property integer $id
 * @property string $permission_name
 * @property string $description
 */
interface Permission extends Model
{

    public function __toString();
}
