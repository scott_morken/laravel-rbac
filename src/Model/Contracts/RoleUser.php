<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:51 PM
 */

namespace Smorken\Rbac\Model\Contracts;

/**
 * Interface RoleUser
 * @package Smorken\Rbac\Model\Contracts
 *
 * @property integer $role_id
 * @property string $user_id
 */
interface RoleUser
{

}
