<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 1:51 PM
 */

namespace Smorken\Rbac\Model\Contracts;

use Smorken\Repositories\Model\Contracts\Model;

/**
 * Interface Role
 * @package Smorken\Rbac\Model\Contracts
 *
 * @property integer $id
 * @property string $role_name
 * @property string $description
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string|null $inherit_from
 * @property boolean $super_admin
 *
 * @property Permission[] $permissions
 * @property RoleUser[] $roleUsers
 */
interface Role extends Model
{

    public function setRoleNameAttribute($value);

    public function inheritsFrom($model = null, $inherits = []);

    public function loadInheritsFrom($name);

    public function inheritsFromList($currentId = null);

    public function getPermissionsListAttribute();
}
