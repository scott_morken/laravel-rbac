<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 11:04 AM
 */

namespace Smorken\Rbac\Model\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleUser
 * @package Smorken\Rbac\Model\Eloquent
 * @property integer $role_id
 * @property string $user_id
 */
class RoleUser extends Model implements \Smorken\Rbac\Model\Contracts\RoleUser
{

    protected $fillable = ['role_id', 'user_id'];

    protected $table = 'role_user';
}
