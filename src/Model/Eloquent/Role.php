<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 10:47 AM
 */

namespace Smorken\Rbac\Model\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Role
 * @package Smorken\Rbac\Model\Eloquent
 * @property integer $id
 * @property string $role_name
 * @property string $description
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string|null $inherit_from
 *
 * @property Permission[] $permissions
 * @property RoleUser[] $roleUser
 */
class Role extends Model implements \Smorken\Rbac\Model\Contracts\Role
{

    use \Smorken\Repositories\Model\Traits\Model;

    protected $rules = [
        'role_name' => 'required',
    ];

    protected $appends = ['permissions_list'];

    protected $fillable = ['role_name', 'description', 'inherit_from', 'super_admin'];

    public function name()
    {
        return $this->role_name;
    }

    public function permissions()
    {
        return $this->belongsToMany('Smorken\Rbac\Model\Eloquent\Permission');
    }

    public function roleUsers()
    {
        return $this->hasMany('Smorken\Rbac\Model\Eloquent\RoleUser');
    }

    public function __toString()
    {
        return sprintf("%s: %s (%s)", $this->id, $this->role_name, $this->description);
    }

    public function setRoleNameAttribute($value)
    {
        $this->attributes['role_name'] = Str::slug($value);
        return true;
    }

    public function inheritsFrom($model = null, $inherits = [])
    {
        if ($model === null && empty($inherits)) {
            $model = $this;
        }
        $inherits[$model->id] = $model;
        if ($model->inherit_from) {
            $new_model = $this->loadInheritsFrom($model->inherit_from);
            if ($new_model) {
                return $this->inheritsFrom($new_model, $inherits);
            }
        }
        return $inherits;
    }

    public function loadInheritsFrom($name)
    {
        $key = 'rbac/inherits/' . $this->id . $name;
        if (!\Cache::has($key)) {
            $query = $this->newQuery();
            $result = $query->where('role_name', '=', $name)->first();
            \Cache::add($key, $result, 60);
        }
        return \Cache::get($key, null);
    }

    public function inheritsFromList($currentId = null)
    {
        $key = 'rbac/inheritslist/' . $this->id . $currentId;
        if (!\Cache::has($key)) {
            $query = $this->newQuery();
            $query->where('super_admin', '=', 0);
            if ($currentId) {
                $query->where('id', '<>', $currentId);
            }
            $query->orderBy('role_name', 'asc');
            $r = $query->get();
            $list = [
                '' => 'None',
            ];
            foreach ($r as $row) {
                $list[$row->role_name] = $row->role_name . ($row->description ? ' (' . $row->description . ')' : '');
            }
            \Cache::add($key, $list, 60);
        }
        return \Cache::get($key, []);
    }

    public function getPermissionsListAttribute()
    {
        $list = [];
        foreach ($this->permissions as $permission) {
            $list[] = $permission->id;
        }
        return $list;
    }
}
