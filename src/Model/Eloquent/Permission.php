<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 10:48 AM
 */

namespace Smorken\Rbac\Model\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Permission
 * @package Smorken\Rbac\Model\Eloquent
 * @property integer $id
 * @property string $permission_name
 * @property string $description
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class Permission extends Model implements \Smorken\Rbac\Model\Contracts\Permission
{

    use \Smorken\Repositories\Model\Traits\Model;

    protected $rules = [
        'permission_name' => 'required',
    ];

    protected $fillable = ['permission_name', 'description'];

    public function setPermissionNameAttribute($value)
    {
        $this->attributes['permission_name'] = Str::slug($value);
        return true;
    }

    public function name()
    {
        return $this->permission_name;
    }

    public function __toString()
    {
        return sprintf("%s: %s (%s)", $this->id, $this->permission_name, $this->description);
    }
}
