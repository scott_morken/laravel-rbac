<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/9/14
 * Time: 10:48 AM
 */
namespace Smorken\Rbac\Controllers;

use Smorken\ControllerTraited\TraitedController as Controller;
use Smorken\Rbac\StorageService;

class BaseController extends Controller
{

    protected $package = 'smorken/rbac::';

    protected $base = '';

    protected $subnav = '';

    /**
     * @var \Smorken\Rbac\StorageService
     */
    protected $storageService;

    protected $provider;

    public function __construct(StorageService $storageService)
    {
        parent::__construct();
        $this->storageService = $storageService;
        $this->initStorageProvider();
    }

    protected function initStorageProvider()
    {
    }

    public function storageService()
    {
        return $this->storageService;
    }
}
