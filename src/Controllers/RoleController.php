<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 8:37 AM
 */

namespace Smorken\Rbac\Controllers;

class RoleController extends BaseController
{

    /**
     * @var \Smorken\Rbac\Storage\RoleRepository
     */
    protected $provider;

    protected $base = 'role';

    protected $subnav = 'admin';

    public function getIndex()
    {
        $roles = $this->getProvider()->loadAllWithPermissions();
        \View::share('operations', static::ops());
        $this->layout->content = \View::make($this->getViewName('index'))
                                      ->with('roles', $roles);
    }

    public function getCreate()
    {
        \View::share('operations', static::ops());
        $this->layout->content = \View::make($this->getViewName('create'))
                                      ->with('model', $this->getProvider()->getModel())
                                      ->with('permissions', $this->storageService()->permissionsProvider()->all());
    }

    public function postCreate()
    {
        $model = $this->provider->create(\Input::only('role_name', 'description', 'super_admin', 'inherit_from'));
        $repo = $this->handlePermissions($model);
        return $this->handlePostSaveRedirects($repo);
    }

    protected function handlePermissions($model)
    {
        $repo = $this->storageService()->rolesProvider(true);
        $repo->setModel($model);
        $result = $repo->handlePermissions(\Input::get('permissions_list'));
        if ($result) {
            $repo->validateAndSave();
        }
        return $repo;
    }

    public function getUpdate($id)
    {
        $model = $this->loadModel($id);
        \View::share('operations', static::ops($id));
        $this->layout->content = \View::make($this->getViewName('update'))
                                      ->with('model', $model)
                                      ->with('permissions', $this->storageService()->permissionsProvider()->all());
    }

    public function postUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->getProvider()->update($model, \Input::only('role_name', 'description', 'super_admin', 'inherit_from'));
        $repo = $this->handlePermissions($model);
        return $this->handlePostSaveRedirects($repo);
    }

    protected function initStorageProvider()
    {
        $this->provider = $this->storageService()->rolesProvider();
    }
}
