<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/15/14
 * Time: 8:37 AM
 */
namespace Smorken\Rbac\Controllers;

class PermissionController extends BaseController
{

    /**
     * @var \Smorken\Rbac\Storage\PermissionRepository
     */
    protected $provider;

    protected $base = 'permission';

    protected $subnav = 'admin';

    public function postCreate()
    {
        return $this->postCreateDefault(\Input::only('permission_name', 'description'));
    }

    public function postUpdate($id)
    {
        return $this->postUpdateDefault($id, \Input::only('permission_name', 'description'));
    }

    protected function initStorageProvider()
    {
        $this->provider = $this->storageService()->permissionsProvider();
    }
}
