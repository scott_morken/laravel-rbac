<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 10:02 AM
 */

namespace Smorken\Rbac;

use Smorken\Rbac\Storage\PermissionRepository;
use Smorken\Rbac\Storage\RoleRepository;

class StorageService
{

    /**
     * @var RoleRepository
     */
    protected $rolesProvider;

    /**
     * @var PermissionRepository
     */
    protected $permissionsProvider;

    /**
     * @param RoleRepository $rolesProvider
     * @param PermissionRepository $permProvider
     */
    public function __construct(RoleRepository $rolesProvider, PermissionRepository $permProvider)
    {
        $this->setRolesProvider($rolesProvider);
        $this->setPermissionsProvider($permProvider);
    }

    /**
     * @param RoleRepository $rolesProvider
     */
    public function setRolesProvider($rolesProvider)
    {
        $this->rolesProvider = $rolesProvider;
    }

    /**
     * @param PermissionRepository $permissionsProvider
     */
    public function setPermissionsProvider($permissionsProvider)
    {
        $this->permissionsProvider = $permissionsProvider;
    }

    /**
     * @param bool $new
     * @return RoleRepository
     */
    public function rolesProvider($new = false)
    {
        if ($new) {
            return clone $this->rolesProvider;
        }
        return $this->rolesProvider;
    }

    /**
     * @param bool $new
     * @return PermissionRepository
     */
    public function permissionsProvider($new = false)
    {
        if ($new) {
            return clone $this->permissionsProvider;
        }
        return $this->permissionsProvider;
    }
}
