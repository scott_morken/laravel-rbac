<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:24 AM
 */

namespace Smorken\Rbac;

use Illuminate\Support\Facades\Auth;

class RbacService
{

    /**
     * @var \Smorken\Rbac\StorageService
     */
    protected $storageService;

    /**
     * $roles is an iterable of models used by the RoleProvider
     * @var array $roles
     */
    protected $roles = null;

    /**
     * array of ids
     * @var array $userRoles
     */
    protected $userRoles = [];

    protected $user_id;

    /**
     * id => name
     * @var array $roleMap
     */
    protected $roleMap;

    /**
     * role_id => array( id => name )
     * @var array $permissionMap
     */
    protected $permissionMap = null;

    /**
     * id => array('name')
     * @var array $inheritance
     */
    protected $inheritance;

    protected $initialized = false;

    public function __construct(StorageService $storageService)
    {
        $this->setStorageService($storageService);
    }

    public function setStorageService(StorageService $storageService)
    {
        $this->storageService = $storageService;
    }

    /**
     * @param $role
     * @param $permission
     * @return bool
     */
    public function hasPermission($role, $permission)
    {
        if ($this->hasRole($role)) {
            $role = $this->mapRoleToId($role);
            $roleModel = $this->allRoles()[$role];
            if ($this->rolesProvider()->isSuperAdmin($roleModel)) {
                return true;
            }
            $permissions = $this->permissionsMapFromArray($roleModel->permissions());
            if (!$permission) {
                return false;
            }
            return $this->_hasPermission($permission, $permissions);
        }
    }

    public function hasRole($role, $inherited = true, $user = null)
    {
        $id = $this->mapRoleToId($role);
        if (!$id) {
            return false;
        }
        if ($this->isUserSuperAdmin($user)) {
            return true;
        }
        $has = $this->_hasRole($id, $user);
        if (!$has && $inherited) {
            $has = $this->hasInheritedRole($id, $user);
        }
        return $has;
    }

    public function mapRoleToId($role)
    {
        $roleint = (int)$role;
        if ($role && (string)$roleint == $role) {
            return $role;
        }
        if (count($this->allRoles()) && array_key_exists(strtolower($role), $this->roleMap())) {
            return (int)$this->roleMap()[$role];
        }
        return false;
    }

    public function allRoles()
    {

        if ($this->roles === null) {
            $this->roles = [];
            $roles = $this->rolesProvider()->loadAllWithPermissions();
            foreach ($roles as $role) {
                $this->addRole($role);
            }
            $this->initRoleMap();
        }
        return $this->roles;
    }

    public function rolesProvider()
    {
        return $this->storageService->rolesProvider();
    }

    public function addRole($role)
    {
        $this->roles[$this->rolesProvider()->id($role)] = $role;
    }

    public function initRoleMap()
    {
        $this->roleMap = [];
        foreach ($this->allRoles() as $model) {
            $id = $this->rolesProvider()->id($model);
            $this->roleMap[strtolower($this->rolesProvider()->name($model))] = $id;
            $this->addInheritance($id, $model);
        }
        $this->initPermissionMap();
        $this->initialized = true;
    }

    /**
     * @param $id
     * @param $model
     */
    public function addInheritance($id, $model)
    {
        $t = $this->rolesProvider()->inheritsFrom($model);
        if (count($t) && !empty($t)) {
            $reversed = array_reverse($t);
            $this->recursiveInheritance(array_pop($reversed), $reversed);
        }
    }

    public function recursiveInheritance($pop, $remaining)
    {
        $id = $this->rolesProvider()->id($pop);
        $current = isset($this->inheritance[$id]) ? $this->inheritance[$id] : [];
        $add[$id] = $pop;
        if ($remaining) {
            foreach ($remaining as $model) {
                $m_id = $this->rolesProvider()->id($model);
                $add[$m_id] = $model;
            }
        }
        $this->inheritance[$id] = $add + $current;
        if ($remaining) {
            $this->recursiveInheritance(array_pop($remaining), $remaining);
        }
    }

    public function initPermissionMap()
    {
        $key = 'rbac/permissions/all';
        if (!\Cache::has($key)) {
            $permissions = $this->permissionsProvider()->all();
            \Cache::add($key, $permissions, 60);
        }
        $permissions = \Cache::get($key, []);
        $this->permissionMap = $this->permissionsMapFromArray($permissions);
    }

    public function permissionsProvider()
    {
        return $this->storageService->permissionsProvider();
    }

    protected function permissionsMapFromArray($permissions)
    {
        $permissionMap = [];
        foreach ($permissions as $permission) {
            $row = $this->permissionsRowFromModel($permission);
            $permissionMap[$row[0]] = $row[1];
        }
        return $permissionMap;
    }

    protected function permissionsRowFromModel($model)
    {
        return [
            strtolower($this->permissionsProvider()->name($model)),
            $this->permissionsProvider()->id($model),
        ];
    }

    public function roleMap()
    {
        if (!$this->initialized) {
            $this->initRoleMap();
        }
        return $this->roleMap;
    }

    public function isUserSuperAdmin($user = null)
    {
        foreach ($this->userRoles($user) as $role) {
            $role = $this->mapRoleToId($role);
            $super = $this->isRoleSuperAdmin($role);
            if ($super == true) {
                return true;
            }
        }
        return false;
    }

    public function userRoles($user = null)
    {
        if ($user === null) {
            $user = Auth::user();
        }
        if ($user) {
            if (is_string($user)) {
                $user_id = $user;
            } else {
                $user_id = $user->getAuthIdentifier();
            }
            if ($this->userId() !== $user_id || !count($this->userRoles)) {
                $this->setUserRoles($this->rolesProvider()->loadUserRoles($user_id));
                $this->user_id = $user_id;
            }
        }
        return $this->userRoles;
    }

    public function userId()
    {
        return $this->user_id;
    }

    public function setUserRoles($roles)
    {
        $this->userRoles = !empty($roles) ? $roles : [];
    }

    public function isRoleSuperAdmin($role)
    {
        if ($role) {
            $roles = $this->allRoles();
            if (isset($roles[$role])) {
                return $this->rolesProvider()->isSuperAdmin($roles[$role]);
            }
        }
        return false;
    }

    protected function _hasRole($role, $user = null)
    {
        if (!$role) {
            return false;
        } else {
            if (is_integer($role)) {
                return in_array($role, $this->userRoles($user));
            }
        }
        return false;
    }

    public function hasInheritedRole($role, $user = null)
    {
        $has = false;
        $roles = $this->userRoles($user);
        foreach ($roles as $role_id) {
            if (isset($this->inheritance()[$role_id]) && isset($this->inheritance()[$role_id][$role])) {
                return true;
            }
        }
        return $has;
    }

    public function inheritance()
    {
        if (!$this->initialized) {
            $this->initRoleMap();
        }
        return $this->inheritance;
    }

    protected function _hasPermission($permission, $permissions)
    {
        if (is_integer($permission)) {
            return in_array($permission, $permissions);
        } else {
            if (is_string($permission)) {
                return $this->_hasPermission($this->mapPermissionToId($permission), $permissions);
            }
        }
        return false;
    }

    public function mapPermissionToId($permission)
    {
        $permint = (int)$permission;
        if ($permission && (string)$permint == $permission) {
            return $permission;
        }
        if (count($this->allRoles()) && array_key_exists(strtolower($permission), $this->permissionMap())) {
            return (int)$this->permissionMap()[$permission];
        }
        return false;
    }

    public function permissionMap()
    {
        return $this->permissionMap;
    }

    public function setAllRoles($roles)
    {
        $this->roles = !empty($roles) ? $roles : [];
    }

    public function setUserId($value)
    {
        $this->user_id = $value;
    }
}
