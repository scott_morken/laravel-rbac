<?php namespace Smorken\Rbac\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RbacSuperCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rbac:super';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registers a user as a super admin.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $storage = app('rbac.storage');
        $repo = $storage->rolesProvider();
        $sarole = $repo->getSuperAdminRole();
        if (!$sarole) {
            $this->error('No super admin role found in repository. Did you create it?');
            return 1;
        }
        $user_id = $this->argument('user_id');
        $repo->addUsersToRole($sarole, [$user_id]);
        if ($repo->errors() === false) {
            $this->info("User [$user_id] added with a role of super admin.");
        } else {
            $this->error("Unable to add user.");
            foreach ($repo->errors()->all() as $message) {
                $this->info($message);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['user_id', InputArgument::REQUIRED, 'User ID to make super admin.'],
        ];
    }
}
