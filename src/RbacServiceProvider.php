<?php namespace Smorken\Rbac;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class RbacServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/views');
        $this->publishes(
            [
                __DIR__ . '/../config' => config_path() . '/vendor/smorken/rbac',
            ],
            'config'
        );
        $this->publishes(
            [
                __DIR__ . '/../database' => base_path() . '/database',
            ],
            'migrations'
        );
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path() . '/resources/views/vendor/smorken/rbac',
            ],
            'views'
        );
        $this->loadRoutes();
    }

    protected function loadRoutes()
    {
        $routefile = __DIR__ . '/../routes.php';
        if (file_exists($routefile)) {
            include $routefile;
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerResources();
        $this->bindRoleProvider();
        $this->bindPermissionProvider();
        $this->app['rbac.storage'] = $this->app->share(
            function ($app) {
                $rp = App::make('Smorken\Rbac\Storage\RoleRepository');
                $pp = App::make('Smorken\Rbac\Storage\PermissionRepository');
                return new StorageService($rp, $pp);
            }
        );

        $this->app->bind(
            'rbac.rules',
            function ($app) {
                $rbac = $app['rbac'];
                $guard = $app['Illuminate\Contracts\Auth\Guard'];
                return new Rules($guard, $rbac);
            }
        );

        $this->app['rbac'] = $this->app->share(
            function ($app) {
                $storage = $app['rbac.storage'];
                return new RbacService($storage);
            }
        );
    }

    protected function registerResources()
    {
        $userconfigfile = config_path() . '/vendor/smorken/rbac/config.php';
        $packageconfigfile = __DIR__ . '/../config/config.php';
        $this->registerConfig($packageconfigfile, $userconfigfile, 'smorken/rbac::config');
    }

    protected function registerConfig($packagefile, $userfile, $namespace)
    {
        $config = $this->app['files']->getRequire($packagefile);
        if (file_exists($userfile)) {
            $userconfig = $this->app['files']->getRequire($userfile);
            $config = array_replace_recursive($config, $userconfig);
        }
        $this->app['config']->set($namespace, $config);
    }

    public function bindRoleProvider()
    {
        $this->app->bind(
            'Smorken\Rbac\Storage\RoleRepository',
            function ($app) {
                $mclass = $app['config']->get('smorken/rbac::config.roles.model');
                $model = new $mclass;
                $providerclass = $app['config']->get('smorken/rbac::config.roles.storage');
                return new $providerclass($model);
            }
        );
    }

    public function bindPermissionProvider()
    {
        $this->app->bind(
            'Smorken\Rbac\Storage\PermissionRepository',
            function ($app) {
                $mclass = $app['config']->get('smorken/rbac::config.permissions.model');
                $model = new $mclass;
                $providerclass = $app['config']->get('smorken/rbac::config.permissions.storage');
                return new $providerclass($model);
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['rbac', 'rbac.storage', 'rbac.rules'];
    }
}
