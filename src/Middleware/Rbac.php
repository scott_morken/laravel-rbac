<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 2:10 PM
 */

namespace Smorken\Rbac\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Smorken\Rbac\Rules;

class Rbac implements Middleware
{

    protected $currAction;

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var Rules
     */
    protected $rules;

    public function __construct(Guard $auth, Rules $rules)
    {
        $this->rules = $rules;
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $action = $route->getAction();
        if (isset($action['rbac'])) {
            $rules = $action['rbac'];
            if ($rules) {
                $this->setCurrentAction($route);
                $allowed = $this->rules->check($this->currAction, $rules);
                if (!$allowed) {
                    abort($this->rules->getCode(), $this->rules->getMessage());
                }
            }
        }
        return $next($request);
    }

    public function setCurrentAction($route)
    {
        $actionName = $route->getActionName();
        $this->currAction = last(explode('@', $actionName));
    }
}
